({
    doInit : function(component, event, helper) {
        var action2 = component.get("c.getLienMap");
        action2.setParams({"aId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                component.set("v.lienMap", response.getReturnValue());
            } 
            else
                console.log('Problem getting lien map, response state: ' + state);
        });
        $A.enqueueAction(action2);
    }    
})