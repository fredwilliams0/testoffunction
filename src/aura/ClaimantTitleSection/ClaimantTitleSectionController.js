/*  Created: lmsw - January 2017 first draft
    Purpose: PD-168 - Title Section on the Claimant Detail Page

History:

	Updated: lmsw - February 2017 March Release
	Purpose: PD-168 - Refresh detail record when change the Claimant status

	Updated: lmsw - February 2017 for March Release
	Purpose: PRODSUPT-14 - remove "Send Email" button

 */
({
	doInit : function(component, event, helper) {
		// Set up Claimant Status dropdown
		var action = component.get("c.getClaimantStatusPicklist");
		var inputsel = component.find("ClaimantStatusSelect");
		var opts=[];
		
		// Create Claimant Status Button options		
		action.setCallback(this, function(a) {
			for(var i=0; i<a.getReturnValue().length; i++) {
				opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
			}
			inputsel.set("v.options", opts);
		});
		$A.enqueueAction(action);
		
		helper.getClaimantRecord(component);
	},
	onStatusChange: function(component, event, helper) {
		// Change status on record depending on selection in picklist
		var claimantId = component.get("v.recordId");
		var selected = component.find("ClaimantStatusSelect").get("v.value");
		
		// change Claimant_status__c value = selected on record
		var action = component.get("c.updateClaimant");
		action.setParams({
			"cId" : claimantId,
			"status"  : selected 
		});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				component.set("v.claimant", response.getReturnValue());
				// Determine color of Status circle during selection
				helper.statusCircle(component);
			} else {
				console.log('Problem getting claimant, response state: ' + state);
			}
// Add refresh
			$A.get("e.force:refreshView").fire();
		});
		$A.enqueueAction(action);
	},
// start PRODSUPT-14
/*	sendEmail : function(component) {
		var lawFirmEmail = component.get("v.claimant.Law_Firm__r.Email__c");
		var claimantName = component.get("v.claimant.Name");
		var link = "mailto:"+lawFirmEmail+"?Subject=About%20"+claimantName;
		window.location.href = link;
	}, */
// end PRODSUPT-14
	editClaimant : function(component, event, helper) {
		var editRecordEvent = $A.get("e.force:editRecord");
		editRecordEvent.setParams({"recordId": component.get("v.claimant.Id")
		});
		editRecordEvent.fire();
	}
})