/*
	Created: RAP - February 2017 for March Release
    Purpose: PD-159 - Action - Lien Summary Tab
*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getStageMetrics");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
	            var sMap = response.getReturnValue();
	            var result = [];
	            var temp = [];
	            var int;
	            for (var plif in sMap) {
	            	temp = sMap[plif];
	       			result.push({
	       				key: plif,
	       				value: temp
	       			});
	            }
	            component.set("v.liensByStage", result);
            } else {
                console.log('Problem getting stage metrics, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        helper.getLienStageLabels(component);
    }
})