/*
	Created: RAP - February 2017 for March Release
    Purpose: PD-159 - Action - Lien Summary Tab
*/
({
	getLienStageLabels : function (component) {
        var action = component.get("c.getLienStageMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var lsMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in lsMap) {
                    temp = lsMap[plif];
                    result.push(temp);
                }
                component.set("v.lienStages", result);
            } else {
                console.log('Problem getting Lien Stage Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	}
})