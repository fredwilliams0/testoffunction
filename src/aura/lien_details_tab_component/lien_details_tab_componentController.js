({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLien");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lien", response.getReturnValue());
            } else {
                console.log('Problem getting lien, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action);
       
    }
,
    showEditLien : function(component, event, helper) {
        document.getElementById("lienEditId").style.display = "block";
        document.getElementById("lienViewId").style.display = "none";
    }
,
    showLienData : function(component, event, helper) {
        document.getElementById("lienEditId").style.display = "none";
        document.getElementById("lienViewId").style.display = "block";
    }
})