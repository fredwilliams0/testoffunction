({
	getLienStageLabels : function (component) {
		// Lien Stage Map apiValue - Label
        var action = component.get("c.getLienStageMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var lsMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in lsMap) {
                    temp = lsMap[plif];
                    console.log("Key/Temp: "+plif+'/'+temp);
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.lienStages", result);
            } else {
                console.log('Problem getting Lien Stage Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	},
    getLienSubstageLabels : function (component) {
        // Lien Stage Map apiValue - Label
        var action = component.get("c.getLienSubstageMap");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var lsMap = response.getReturnValue(); 
                var result = [];
                var temp = [];
                for (var plif in lsMap) {
                    temp = lsMap[plif];
                    console.log("Key/Temp: "+plif+'/'+temp);
                    result.push({
                        key: plif,
                        value: temp
                    });
                }
                component.set("v.lienSubstages", result);
            } else {
                console.log('Problem getting Lien Stage Map, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})