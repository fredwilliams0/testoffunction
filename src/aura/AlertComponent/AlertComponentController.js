({
    doInit : function(component, event, helper) {
        var action = component.get("c.getAlerts");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
	            component.set("v.alerts", response.getReturnValue());
            } else {
                console.log('Problem getting alerts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})