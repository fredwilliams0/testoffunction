({
    doInit : function(component, event, helper) {
        var action = component.get("c.getAwards");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.awards", response.getReturnValue());
            } else {
                console.log('Problem getting awards, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }

, 
    editAward : function(component, event, helper) {
        var awardrowID = event.target.id;
        var editRecordEvent = $A.get("e.force:editRecord");
            editRecordEvent.setParams({
                "recordId": awardrowID
            });
        editRecordEvent.fire();
    } 
})