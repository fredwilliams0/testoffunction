/*
    Created: RAP - March 2017 for March Release
    Purpose: Test functions of List Controllers
    		 Coverage as of 3/7 - LienBatch: 100%
    		 					  DistributionBatch: 96%
*/
@isTest
private class Batch_Test {
    
@testSetup
	static void CreateData() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
        Account acc = new Account(Name = 'Law Firm',
        						  RecordTypeId = rtId,
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
        Action__c action = new Action__c(Name = 'Test Action',
                                         Law_Firm__c = acc.Id,
                                         Active__c = false);
        insert action;
        DisbursementDates__c ddc = new DisbursementDates__c(Name = action.Id,
        													Date__c = system.today());
        insert ddc;
        Claimant__c c = new Claimant__c(Address__c = '123 Main St',
										City__c = 'Denver',
										Email__c = 'rap@rap.com',
										Law_Firm__c = acc.Id,
										First_Name__c = 'RAP',
										Last_Name__c = 'RAPPER',
										Phone__c = '3035551212',
										SSN__c = '135461448',
										State__c = 'CO',
										Status__c = 'Not_Ready_to_be_Cleared',
										Zip__c = '80138');
        insert c;
		Award__c a1 = new Award__c(Claimant__c = c.Id,
								   Action__c = action.Id,
								   Date_of_Award__c = system.today(), 
								   Description__c = 'Award #1', 
								   Amount__c = 190000, 
								   Distribution_Status__c = 'Released_to_a_Holdback');
		Award__c a2 = new Award__c(Claimant__c = c.Id,
								   Action__c = action.Id,
								   Date_of_Award__c = system.today(), 
								   Description__c = 'Award #2', 
								   Amount__c = 240000, 
								   Distribution_Status__c = 'Cleared');
		insert new list<Award__c>{a1,a2};
        Lien__c l1 = new Lien__c(Account__c = acc.Id, 
                                 Action__c = action.Id, 
                                 Claimant__c = c.Id, 
                                 Cleared__c = false,
                                 Lien_Type__c = 'Private', 
                                 Notes__c = 'This is a note', 
                                 Stages__c = 'Final', 
                                 State__c = 'CO', 
                                 Submitted__c = true);
        Lien__c l2 = new Lien__c(Account__c = acc.Id, 
                                 Action__c = action.Id, 
                                 Claimant__c = c.Id, 
                                 Cleared__c = false,
                                 Lien_Type__c = 'Medicaid', 
                                 Notes__c = 'This is also a note', 
                                 Stages__c = 'Final', 
                                 State__c = 'CO', 
                                 Submitted__c = true);
		insert new list<Lien__c>{l1,l2};
		Lien_Negotiated_Amounts__c lna1 = new Lien_Negotiated_Amounts__c(Lien__c = l1.Id,
																		 Lien_Amount__c = 23425.77,
																		 Lien_Amount_Date__c = system.today(),
																		 Phase__c = 'Final');
		Lien_Negotiated_Amounts__c lna2 = new Lien_Negotiated_Amounts__c(Lien__c = l2.Id,
																		 Lien_Amount__c = 21312.56,
																		 Lien_Amount_Date__c = system.today(),
																		 Phase__c = 'Final');
		insert new list<Lien_Negotiated_Amounts__c>{lna1,lna2};
		Injury__c i1 = new Injury__c(Name = 'Injury1',
									 Action__c = action.Id, 
                                 	 Claimant__c = c.Id,
									 Compensable__c = 'True', 
									 DOL__c = system.today().addDays(-45));
		Injury__c i2 = new Injury__c(Name = 'Injury2',
									 Action__c = action.Id, 
									 Claimant__c = c.Id,
									 Compensable__c = 'True', 
									 DOL__c = system.today().addDays(-45));
		Injury__c i3 = new Injury__c(Name = 'Injury3',
									 Action__c = action.Id, 
									 Claimant__c = c.Id,
									 Compensable__c = 'True', 
									 DOL__c = system.today().addDays(-45));
		Injury__c i4 = new Injury__c(Name = 'Injury4',
									 Action__c = action.Id, 
									 Claimant__c = c.Id,
									 Compensable__c = 'True', 
									 DOL__c = system.today().addDays(-45));
		insert new list<Injury__c>{i1,i2,i3,i4};
		InjuryAward__c ia1 = new InjuryAward__c(Injury__c = i1.Id,
												Award__c = a1.Id);
		InjuryAward__c ia2 = new InjuryAward__c(Injury__c = i2.Id,
												Award__c = a1.Id);
		InjuryAward__c ia3 = new InjuryAward__c(Injury__c = i3.Id,
												Award__c = a2.Id);
		InjuryAward__c ia4 = new InjuryAward__c(Injury__c = i4.Id,
												Award__c = a2.Id);
		insert new list<InjuryAward__c>{ia1,ia2,ia3,ia4};
		Award_Cost_Deduction__c acd1 = new Award_Cost_Deduction__c(Award__c = a1.Id,
																   Amount__c = 255,
																   Cost_Deduction_Type__c = 'Court_Fee');
		Award_Cost_Deduction__c acd2 = new Award_Cost_Deduction__c(Award__c = a1.Id,
																   Amount__c = 2500,
																   Cost_Deduction_Type__c = 'Award_Deduction');
		Award_Cost_Deduction__c acd3 = new Award_Cost_Deduction__c(Award__c = a2.Id,
																   Amount__c = 255,
																   Cost_Deduction_Type__c = 'Court_Fee');
		Award_Cost_Deduction__c acd4 = new Award_Cost_Deduction__c(Award__c = a2.Id,
																   Amount__c = 2500,
																   Cost_Deduction_Type__c = 'Award_Deduction');
		insert new list<Award_Cost_Deduction__c>{acd1,acd2,acd3,acd4};
		AwardLien__c al1 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = l1.Id, 
											Award__c = a1.Id);
		AwardLien__c al2 = new AwardLien__c(PercentToAward__c = 0.5, 
											Lien__c = l1.Id, 
											Award__c = a2.Id);
		AwardLien__c al3 = new AwardLien__c(PercentToAward__c = 1.0, 
											Lien__c = l2.Id, 
											Award__c = a1.Id);
		insert new list<AwardLien__c>{al1,al2,al3};		
	}
	
	static testMethod void TestDistributionBatch() {
		test.startTest();
		string actId = [SELECT Id FROM Action__c WHERE Name = 'Test Action' limit 1].Id;
		database.executeBatch(new DistributionBatch(actId));
		test.stopTest();
	}

	static testMethod void TestLienBatch() {
		test.startTest();
		string actId = [SELECT Id FROM Action__c WHERE Name = 'Test Action' limit 1].Id;
		database.executeBatch(new LienBatch(actId));
		test.stopTest();
	}
}