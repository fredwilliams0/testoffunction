/*
    Created: RAP - March 2017 for March Release
    Purpose: Test functions of Lien Trigger Utility Class - PD-434 and techDebt
    		 Coverage as of 3/6 - 93%
*/
@isTest
private class LienUtility_Test {

@testSetup
	static void CreateData() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
        Account acc = new Account(Name = 'Law Firm',
        						  RecordTypeId = rtId,
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
        Action__c action = new Action__c(Name = 'Test Action',
                                         Active__c = true);
        insert action;
		ActionAssignment2__c aac = new ActionAssignment2__c(Name = userInfo.getUserId(),
															Action__c = action.Id);
		insert aac;
		Time_Processing__c tpc = new Time_Processing__c(Name = action.Id,
														Second_Notice_Interval__c = 30,
														Third_Notice_Interval__c = 30);
		insert tpc; 
        Claimant__c claim = new Claimant__c(Address__c = '123 Main St',
                                            City__c = 'Denver',
                                            Email__c = 'rap@rap.com',
                                            Law_Firm__c = acc.Id,
                                            First_Name__c = 'RAP',
                                            Last_Name__c = 'RAPPER',
                                            Phone__c = '3035551212',
                                            SSN__c = '135461448',
                                            State__c = 'CO',
                                            Status__c = 'Cleared',
                                            Zip__c = '80138');
        insert claim;
        Lien__c lien = new Lien__c(Account__c = acc.Id, 
                                   Action__c = action.Id, 
                                   Claimant__c = claim.Id, 
                                   Cleared__c = false,
                                   Lien_Type__c = 'Private', 
                                   Notes__c = 'This is a note', 
                                   Stages__c = 'To_Be_Submitted', 
                                   State__c = 'CO', 
                                   Submitted__c = false);
        Lien__c lien2 = new Lien__c(Account__c = acc.Id, 
                                    Action__c = action.Id, 
                                    Claimant__c = claim.Id, 
                                    Cleared__c = false,
                                    Lien_Type__c = 'Private', 
                                    Notes__c = 'This is a note', 
                                    Stages__c = 'Submitted', 
                                    State__c = 'CO', 
                                    Submitted__c = true);
		insert new list<Lien__c>{lien,lien2};
	}
	
	static testMethod void TestStageCalcs() {
		test.startTest();
		Lien__c lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, Lien_Type__c, 
							   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
							   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c
						FROM Lien__c
						WHERE Submitted__c = false
						limit 1];
		lien.Stages__c = 'Submitted';
        update lien;
		lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, Lien_Type__c, 
					   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
					   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c
				FROM Lien__c
				WHERE Id = :lien.Id
				limit 1];
		system.assertEquals(system.today(), lien.Date_Submitted__c);
		system.assertEquals(true, lien.Submitted__c);
		system.assertEquals('First_Notice_Sent', lien.Substage__c);
        lien.Date_Submitted__c = system.today().addDays(-15);
        update lien;
        lien.Date_Submitted__c = system.today().addDays(-30);
        update lien;
		lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, Lien_Type__c, 
					   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
					   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c
				FROM Lien__c
				WHERE Id = :lien.Id
				limit 1];
		system.assertEquals(true, lien.Second_Notice_Sent__c);
		system.assertEquals('Second_Notice_Sent', lien.Substage__c);
		system.assertEquals(system.today(), lien.Date_Second_Sent__c);
        lien.Date_Submitted__c = system.today().addDays(-45);
        lien.Date_Second_Sent__c = system.today().addDays(-15);
        update lien;
        lien.Date_Submitted__c = system.today().addDays(-60);
        lien.Date_Second_Sent__c = system.today().addDays(-30);
        update lien;
		lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, Lien_Type__c, 
					   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
					   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c
				FROM Lien__c
				WHERE Id = :lien.Id
				limit 1];
		system.assertEquals(true, lien.Third_Notice_Sent__c);
		system.assertEquals('Third_Notice_Sent', lien.Substage__c);
		system.assertEquals(system.today(), lien.Date_Third_Sent__c);
		lien.Date_Third_Sent__c = system.today().addDays(-10);
		update lien;
		lien.Stages__c = 'Audit';
		update lien;
        lien.Stages__c = 'Final';
        update lien;
        test.stopTest();
	}

	static testMethod void TestOutlyers() {
		test.startTest();
		Lien__c lien = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, Lien_Type__c, 
							   Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
							   Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c, Substage__c
						FROM Lien__c
						WHERE Submitted__c = true
						limit 1];
		lien.Lien_Type__c = 'PLRP_Modeled';
        lien.Second_Notice_Sent__c = true;
        lien.Date_Second_Sent__c = system.today().addDays(-25);
        update lien;
        lien.Stages__c = 'Final';
        update lien;
        test.stopTest();
	}
}