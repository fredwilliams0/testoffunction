/*
    Created: RAP - February 2017 for March Release
    Purpose: PD-173 - Claimant - Award Tab
    
History:

	Updated: RAP - March 2017 for March Release
	Purpose: PD-473 - Claimant Detail Page - Awards Tab - Total Amount remaining
	
*/
public with sharing class AwardListController {

    public Id claimantId{get;set;}
    public list<AwardWrapper> awList{get;set;}
    public decimal totalAwards{get;set;}
    public decimal totalCosts{get;set;}
    public decimal totalHoldbacks{get;set;}
	public map<string,decimal> holdbackMap = new map<string,decimal>();
	public decimal totalLiens{get;set;}
	public list<Award__c> awards {
   		get {
   			if (awards == null && claimantId != null) {
   				awards = [SELECT Name, Date_of_Award__c, Description__c, Amount__c, Distribution_Status__c, 
   								 Assoc_Costs_Award_Deductions__c, Amount_Applied__c,
								 (SELECT Injury__c, Injury__r.Name FROM Injuries__r),
								 (SELECT Amount__c FROM ACD__r),
								 (SELECT PercentToAward__c, Lien__c, Lien__r.HBAmount__c, Lien__r.Final_Lien_Amount__c, 
								 		 Lien__r.Name, LienAmountCovered__c
								  FROM Liens__r)
						  FROM Award__c
						  WHERE Claimant__c = :claimantId];
   			}
   			return awards;
   		}
   		set;
	}
// start PD-473
    public map<string,string> statusMap {
    	get {
	        if (statusMap == null) {
	            statusMap = new map<string,string>();
	            list<Schema.PicklistEntry> pickList = Award__c.Distribution_Status__c.getDescribe().getPicklistValues();
	            for (Schema.PicklistEntry spe : pickList)
	                statusMap.put(spe.getValue(),spe.getLabel());
	        }
	        return statusMap;
    	}
    	set;
    }
// end PD-473

// constructor
	public AwardListController(ApexPages.StandardController stdCtrl) {
		claimantId = stdCtrl.getId();
		totalAwards = 0;
		totalCosts = 0;
		totalLiens = 0;
		totalHoldbacks = 0;
		system.debug(loggingLevel.INFO, 'RAP --->> awards = ' + awards);
   		awList = new list<AwardWrapper>();
   		for (Award__c a : awards) {
   			totalAwards += a.Amount__c;
   			totalCosts += a.Assoc_Costs_Award_Deductions__c;
			totalLiens += a.Amount_Applied__c;
	   		for (AwardLien__c al : a.Liens__r)
				totalHoldbacks += al.Lien__r.HBAmount__c;
   			AwardWrapper awr = new AwardWrapper(a, statusMap);
   			awList.add(awr);
   		}
	}
	public class AwardWrapper {
		public string idVal{get;set;}
		public string name{get;set;}
		public date dateVal{get;set;}
		public string description{get;set;}
		public decimal amount{get;set;}
		public string status{get;set;}
		public decimal totalCosts{get;set;}
		public list<AwardLien__c> liens{get;set;}
		public list<InjuryAward__c> injuries{get;set;}
		public boolean showLiens{get;set;}
		public boolean showInjuries{get;set;}
		public AwardWrapper(Award__c award, map<string,string> statuses) {
			idVal = award.Id;
			name = award.Name;
			dateVal = award.Date_of_Award__c;
			description = award.Description__c;
			amount = award.Amount__c;
			status = statuses.get(award.Distribution_Status__c); // PD-473
			totalCosts = award.Assoc_Costs_Award_Deductions__c;
			liens = award.Liens__r;
			injuries = award.Injuries__r;
			showLiens = liens != null && !liens.isEmpty();
			showInjuries = injuries != null && !injuries.isEmpty();
		}
	}
}