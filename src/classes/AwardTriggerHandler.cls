/*
	Created: MEF - January 2017 for first release
	Purpose: Handler for Award__c trigger events
	ToDo: (proposed) create utility methods possibly to separate classes as appropriate (getActionNames, getClaimantNames, etc...)
*/
public with sharing class AwardTriggerHandler {

	public void onAfterInsert(List<Award__c> newAwardRecs, Map<Id, Award__c> newAwardsMap) {
	
	
	} //endonAfterInsert   
	     
	public void onBeforeUpdate(List<Award__c> newAwardRecs, Map<Id, Award__c> newAwardsMap, Map<Id, Award__c> oldAwardsMap) {

	} //end onBeforeUpdate

	public void onBeforeInsert(List<Award__c> newAwardRecs, Map<Id, Award__c> newAwardsMap) {

	buildAwardName(newAwardRecs, newAwardsMap);
	
	} //end onBeforeInsert

	public void onAfterUpdate(List<Award__c> newAwardRecs, List<Award__c> oldAwardRecs, Map<Id, Award__c> newAwardsMap, Map<Id, Award__c> oldAwardsMap) {


	} //end onAfterUpdate

	private void buildAwardName(List<Award__c> newAwards, Map<Id, Award__c> newAwardMap) {

		List<Action__c> actionList = [SELECT Id, Name FROM Action__c];
		Map<Id, String> actionNameMap = new Map<Id, String>();
		for(Action__c anAction : actionList)
			actionNameMap.put(anAction.Id, anAction.Name);

		List<Id> claimantIdList = new List<Id>();
		for (Award__c theAward :newAwards)
			claimantIdList.add(theAward.Claimant__c);

		List<Claimant__c> claimantList = [SELECT Id, Name FROM Claimant__c Where Id IN :claimantIdList];
		Map<Id, String> claimantNameMap = new Map<Id, String>();
		for(Claimant__c aClaimant : claimantList)
			claimantNameMap.put(aClaimant.Id, aClaimant.Name);

		for(Award__c theAward : newAwards) {
			theAward.Name = claimantNameMap.get(theAward.Claimant__c) 
							 + ' - ' 
							 + theAward.Description__c 
							 + ' - '
							 + actionNameMap.get(theAward.Action__c);
		} //end buildAwardName
		
	} //end ActionTriggerHandler	




} //end class