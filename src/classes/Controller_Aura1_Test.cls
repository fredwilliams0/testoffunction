/*
    Created: RAP - January 2017 for first release
    Purpose: test function of Controller_Aura1
    Coverage as of 1/10/17 - Controller_Aura1: 100%
                           - LienUtility: 100%
                           - Lien Trigger: 100%
    History:
    
    Updated: Lmsw - January 2017 for March Release
    Purpose: add test coverage for claimant, claimantStatusPicklist, andupdateClaimant
    
    Updated: RAP - March 2017 for March Release
    Purpose: test completion/refactor.
*/
@isTest
private class Controller_Aura1_Test {

@testSetup
    static void CreateData() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
        Account a = new Account(Name = 'Law Firm',
        						RecordTypeId = rtId,
        						Priority_Firm__c = true, 
                                BillingStreet = '123 Main St',
                                BillingCity = 'Denver',
                                BillingState = 'CO',
                                BillingPostalCode = '80202');
        Account b = new Account(Name = 'Payable Entity',
                                BillingStreet = '345 Elm St',
                                BillingCity = 'Portland',
                                BillingState = 'OR',
                                BillingPostalCode = '50303');
        insert new list<Account>{a,b};
        Contact c = new Contact(LastName = 'Crain',
                                email = 'denny@crain.com');
        insert c;
        Action__c action = new Action__c(Name = 'Test Action',
                                         Active__c = true,
                                         Law_Firm__c = a.Id,
                                         Law_Firm_Attorney_Contact__c = c.Id);
        insert action;
        Claimant__c claim = new Claimant__c(Address__c = '123 Main St',
                                            City__c = 'Denver',
                                            Email__c = 'rap@rap.com',
                                            Law_Firm__c = a.Id,
                                            Name = 'RAP',
                                            Phone__c = '3035551212',
                                            SSN__c = '135461448',
                                            State__c = 'CO',
                                            Status__c = 'Cleared',
                                            Zip__c = '80138');
        insert claim;
        ActionClaimant__c ac = new ActionClaimant__c(Action__c = action.Id,
                                                     Claimant__c = claim.Id);
        insert ac;
        Id aId = action.Id;
        Id cId = claim.Id;
        Action_Account__c aa1 = new Action_Account__c(Action__c = aId,
        											Account__c = a.Id);
        Action_Account__c aa2 = new Action_Account__c(Action__c = aId,
        											Account__c = b.Id);
        insert new list<Action_Account__c>{aa1,aa2};
        Ledger__c gl = new Ledger__c(Account__c = a.Id, 
                                     Action__c = aId, 
                                     Claimant__c = cId);
        insert gl;
        Lien__c lien1 = new Lien__c(Account__c = a.Id, 
                                    Action__c = aId, 
                                    Claimant__c = cId, 
                                    Cleared__c = false,
                                    Date_Submitted__c = system.today(), 
                                    Ledger__c = gl.Id, 
                                    Lien_Type__c = 'Private', 
                                    Notes__c = 'This is a note', 
                                    Payable_Entity__c = b.Id,
                                    Stages__c = 'Submitted', 
                                    State__c = 'CO', 
                                    Status_ERISA__c = 'MCO', 
                                    Submitted__c = true);
        Lien__c lien2 = new Lien__c(Account__c = a.Id, 
                                    Action__c = aId, 
                                    Claimant__c = cId, 
                                    Cleared__c = false,
                                    Date_Submitted__c = system.today(), 
                                    Ledger__c = gl.Id, 
                                    Lien_Type__c = 'Private', 
                                    Notes__c = 'This is also a note', 
                                    Payable_Entity__c = b.Id,
                                    Stages__c = 'To_Be_Submitted', 
                                    State__c = 'CO', 
                                    Status_ERISA__c = 'MCO', 
                                    Submitted__c = true);
        insert new list<Lien__c>{lien1,lien2};
        map<integer,string> lienMap = new map<integer,string>();
        list<Lien_Negotiated_Amounts__c> lienList = new list<Lien_Negotiated_Amounts__c>();
        list<LienEligibilityDate__c> ledList = new list<LienEligibilityDate__c>();
        list<Injury__c> injList = new list<Injury__c>();
        for (integer j=0; j<4; j++) {
			Injury__c inj1 = new Injury__c(Compensable__c = 'True', 
        								   Action__c = action.Id,
        								   Claimant__c = cId, 
        								   Injury_Category__c = string.valueOf(j), 
        								   Injury_Description__c = 'Injury Most Foul ' + string.valueOf(j), 
										   DOL__c = system.today().addDays(-60));
        	injList.add(inj1);
        }
        insert injList;
        Date today = system.today();
        lienMap.put(0, 'Asserted');
        lienMap.put(1, 'Audit');
        lienMap.put(2, 'Neg - Proposed');
        lienMap.put(3, 'Neg - Response');
        lienMap.put(4, 'Contested');
        lienMap.put(5, 'Final');
        lienMap.put(6, 'Paid');
        for (integer i=0;i<7;i++) {
            string str = lienMap.get(i);
            Date dateStr = today.addMonths(i);
            Lien_Negotiated_Amounts__c lnac = new Lien_Negotiated_Amounts__c(Lien__c = lien1.Id, 
                                                                             Lien_Amount__c = 50000-(i*2500), 
                                                                             Lien_Amount_Date__c = dateStr, 
                                                                             Phase__c = lienMap.get(i));
            lienList.add(lnac);
			LienEligibilityDate__c led = new LienEligibilityDate__c(Date_End__c = system.today().addDays(i),
        															Date_Start__c = system.today().addDays(-i),
        															Lien__c = lien2.Id, 
        															LienType__c = 'Part A');
        	ledList.add(led);
			
        }
        insert lienList;
        insert ledList;
       	InjuryLien__c il1 = new InjuryLien__c(Injury__c = injList[0].Id,
        									  Lien__c = lien1.Id);
       	InjuryLien__c il2 = new InjuryLien__c(Injury__c = injList[1].Id,
        									  Lien__c = lien1.Id);
       	InjuryLien__c il3 = new InjuryLien__c(Injury__c = injList[2].Id,
        									  Lien__c = lien2.Id);
       	InjuryLien__c il4 = new InjuryLien__c(Injury__c = injList[3].Id,
        									  Lien__c = lien2.Id);
        insert new list<InjuryLien__c>{il1,il2,il3,il4};
        Award__c aw = new Award__c(Action__c = aId, 
        						   Claimant__c = cId, 
        						   Date_of_Award__c = system.today(), 
        						   Amount__c = 175000, 
        						   Description__c = 'Description');
        insert aw;
        AwardLien__c aw1 = new AwardLien__c(Award__c = aw.Id,
        									Lien__c = lien1.Id);
        AwardLien__c aw2 = new AwardLien__c(Award__c = aw.Id,
        									Lien__c = lien2.Id);
        insert new list<AwardLien__c>{aw1,aw2};
        InjuryAward__c ia1 = new InjuryAward__c(Injury__c = injList[0].Id,
        										Award__c = aw.Id);
        InjuryAward__c ia2 = new InjuryAward__c(Injury__c = injList[1].Id,
        										Award__c = aw.Id);
        InjuryAward__c ia3 = new InjuryAward__c(Injury__c = injList[2].Id,
        										Award__c = aw.Id);
        InjuryAward__c ia4 = new InjuryAward__c(Injury__c = injList[3].Id,
        										Award__c = aw.Id);
        insert new list<InjuryAward__c>{ia1,ia2,ia3,ia4};
    }
    static testMethod void TestLiens() {
        test.startTest();
        Lien__c l = [SELECT Id, Account__c, Action__c, Claimant__c, Ledger__c, Payable_Entity__c 
                     FROM Lien__c 
                     WHERE Stages__c = 'To_Be_Submitted' limit 1];
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(l);
        Controller_Aura1 ctrl = new Controller_Aura1(stdctrl);
        Lien__c lCheck = Controller_Aura1.getLien(l.Id);
        list<Lien__c> checkLiens = Controller_Aura1.getLiens(string.valueOf(l.Claimant__c), string.valueOf(l.Action__c)); 
        list<Injury__c> iList = Controller_Aura1.getInjuries(l.Id);
        list<Award__c> aList = Controller_Aura1.getAwards(l.Id);
        map<string,list<LienEligibilityDate__c>> ledMap = Controller_Aura1.getLienEligibleDates(l.Id);
        list<Claimant__c> cList = Controller_Aura1.getClaimants(string.valueOf(l.Action__c));
        list<Lien_Negotiated_Amounts__c> l1 = Controller_Aura1.getlienAmtList(l.Id); 
        map<string,list<Lien__c>> lienMap = Controller_Aura1.getLienMap(string.valueOf(l.Action__c));
        lienMap = Controller_Aura1.getLienMap2(string.valueOf(l.Claimant__c));
        map<string,string> lienTypeMap = Controller_Aura1.getLienTypeMap();
        map<string,string> lienStageMap = Controller_Aura1.getLienStageMap();
        map<string,string> lienSubStageMap = Controller_Aura1.getLienSubstageMap();
// bump coverage for LienUtility
        Lien__c lien3 = new Lien__c(Account__c = l.Account__c, 
                                    Action__c = l.Action__c, 
                                    Claimant__c = l.Claimant__c, 
                                    Cleared__c = false,
                                    Date_Submitted__c = system.today(), 
                                    Ledger__c = l.Ledger__c, 
                                    Lien_Type__c = 'Private', 
                                    Notes__c = 'This is a note', 
                                    Payable_Entity__c = l.Payable_Entity__c,
                                    Stages__c = 'Submitted', 
                                    State__c = 'CO', 
                                    Status_ERISA__c = 'MCO', 
                                    Submitted__c = true);
        insert lien3;
//
		Controller_Aura1.updateLienCard('Audit', lien3.Id);
		lien3 = Controller_Aura1.updateLien(lien3.Id, 'Reason', 15456.73);
        test.stopTest();
    }
// Test Claimant    
    static testMethod void TestClaimant() {
        test.startTest();
        Claimant__c c = [SELECT Id, name, email__c, Phone__c, SSN__c, status__c, Law_Firm__c
                         FROM Claimant__c
                         limit 1];
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(c);
        Controller_Aura1 ctrl = new Controller_Aura1(stdctrl);
        Claimant__c cCheck = Controller_Aura1.getClaimant(c.Id);
        system.assertEquals('RAP',[SELECT Id, Name FROM Claimant__c WHERE Id =:c.Id].Name);
        
        List<String> statusList = Controller_Aura1.getClaimantStatusPicklist();
        system.assertEquals(3,statusList.size());

        Controller_Aura1.updateClaimant(cCheck.id, 'Holdback');
        system.assertEquals('Holdback', [SELECT id, status__c FROM Claimant__c WHERE Id =: cCheck.id].status__c);
        test.stopTest();
    }
// Test Injuries and Awards    
    static testMethod void TestInjuriesAndAwards() {
        test.startTest();
        Lien__c l = [SELECT Id, Account__c, Action__c, Claimant__c, Ledger__c, Payable_Entity__c, Claimant__r.Name, 
        					Claimant__r.Email__c, Claimant__r.Phone__c, Claimant__r.SSN__c, Claimant__r.Status__c, 
        					Claimant__r.Law_Firm__c 
                     FROM Lien__c 
                     WHERE Stages__c = 'To_Be_Submitted'
                     limit 1];
        list<Injury__c> injList = Controller_Aura1.getInjuries2(l.Claimant__c);
        list<Award__c> awList = Controller_Aura1.getAwardsByLien(l.Id);
        Claimant__c claimant = Controller_Aura1.updateClaimant(l.Claimant__c, 'Cleared');
        list<Action__c> actionList = Controller_Aura1.getActions();
        Action__c action = Controller_Aura1.getAction(actionList[0].Id);
        list<string> stages = Controller_Aura1.getStages();
		map<string,string> stageMap = Controller_Aura1.getStagesMap();
        list<Lien__c> lienList = Controller_Aura1.getLiensByInjury(injList[0].Id);
        injList = Controller_Aura1.getInjuriesByAward(awList[0].Id);
        map<string,list<Award__c>> awardMap = Controller_Aura1.getAwardsByInjuryMap(l.Claimant__c);
        awList = Controller_Aura1.getAwardsByInjury(injList[0].Id);
        map<string,list<string>> metricMap = Controller_Aura1.getLawFirmMetrics(l.Action__c);
        list<Claimant__c> clList = Controller_Aura1.getLFClaimants(l.Action__c);
        map<string,list<Claimant__c>> clMap = Controller_Aura1.getOthers(l.Action__c);
        map<string,integer> clStatusCount = Controller_Aura1.getClaimantStatusCount(l.Action__c);
        integer clTotal = Controller_Aura1.getClaimantsCountForAction(l.Action__c);
        clStatusCount = Controller_Aura1.getDistributionStatusCount(l.Action__c);
        clTotal = Controller_Aura1.getAwardsCountForAction(l.Action__c);
        map<string,list<string>> stageMetrics = Controller_Aura1.getStageMetrics(l.Action__c);
        test.stopTest();
    }
}