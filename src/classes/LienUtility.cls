/*
	Created: RAP - January 2017 for March Release
	Purpose: techDebt - Move Lien Trigger processing to utility class 

History:

	Updated: RAP - January 2017 for March Release
	Purpose: techDebt - Add trigger.oldMap to process name change on Lein_Type changes 

    Updated: RAP February 2017 for March Release
    Purpose: PD-434 - Tracking time between Lien substages (Batch processor touches all Liens in the action and 1. calculates 
    				  time since submitted, 2. sends email if 30/60 days since submittal.
*/
public without sharing class LienUtility { 

	public static Date queryDate = system.today();
	public static integer queryDay = queryDate.dayOfYear();
		
	public static void ProcessLiens(list<Lien__c> liens, map<Id,Lien__c> oldLiens) { 	   
	    if (trigger.isBefore) {
	    	map<string,Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Lien__c.getRecordTypeInfosByName();
	    	list<Id> cList = new list<Id>();
	    	for (Lien__c l : liens) {
	    		cList.add(l.Claimant__c);
	    	}
		    map<Id,Claimant__c> cMap = new map<Id,Claimant__c>([SELECT Id, Name FROM Claimant__c WHERE Id IN :cList]);
			list<Lien__c> lList = [SELECT Claimant__c, Name 
								   FROM Lien__c 
								   WHERE Claimant__c IN :cList];
			map<Id,string> lienNameMap = new map<Id,string>();
			for (Lien__c l : lList) {
				if (lienNameMap.containsKey(l.Claimant__c)) {
					string name = lienNameMap.get(l.Claimant__c);
					name += ';' + l.Name;
					lienNameMap.put(l.Claimant__c, name);
				}
				else
					lienNameMap.put(l.Claimant__c, l.Name);
			}
	    	for (Lien__c lien : liens) {
	    		if (trigger.isInsert || (trigger.isUpdate && 
	    			(lien.Lien_Type__c != oldLiens.get(lien.Id).Lien_Type__c))) {
			    	if (rtMap.containsKey(lien.Lien_Type__c))
			    		lien.RecordTypeId = rtMap.get(lien.Lien_Type__c).getRecordTypeId();
			    	lien.Name = lien.Lien_Type__c + ' - ' + cMap.get(lien.Claimant__c).Name;
		    		integer i = 1;
		    		for (i=1 ; i < 100 ; i++) {
		    			string suffix = i < 10 ? ' - 0' + string.valueOf(i) : ' - ' + string.valueOf(i);
		    			if (!lienNameMap.containsKey(lien.Claimant__c) || 
		    				!lienNameMap.get(lien.Claimant__c).contains(lien.Name + suffix)) {
		    				lien.Name += suffix;
							if (lienNameMap.containsKey(lien.Claimant__c)) {
								string name = lienNameMap.get(lien.Claimant__c);
								name += ';' + lien.Name;
								lienNameMap.put(lien.Claimant__c, name);
							}
							else
								lienNameMap.put(lien.Claimant__c, lien.Name);
		    				break;
		    			}
		    		}
			    }
		    }
// start PD-434
		    if (trigger.isUpdate) {
		    	ActionAssignment2__c act = ActionAssignment2__c.getInstance(userInfo.getUserId());
				if (act == null)
					return;
				Time_Processing__c tpc = Time_Processing__c.getInstance(act.Action__c);
				if (tpc == null)
					return;
				LienTypeIntervals__c lti;
				list<Id> ids1 = new list<Id>();
				list<Id> ids2 = new list<Id>();
				list<Id> ids3 = new list<Id>();
				integer interval;
		    	for (Lien__c l : liens) { 
					integer compare;
					boolean ltiB = false;
					if (tpc.LienTypeInterval__c)
						lti = LienTypeIntervals__c.getInstance(l.Lien_Type__c);
					if (lti != null) {
						compare = integer.valueOf(lti.Interval__c);
						ltiB = true;
					}
					if (l.Stages__c == 'Submitted') {
						if (oldLiens.get(l.Id).Stages__c != 'Submitted') {
							ids1.add(l.Id);
							l.Date_Submitted__c = queryDate;
							l.Submitted__c = true;
							l.Substage__c = 'First_Notice_Sent';
							continue;
						}
						system.debug(loggingLevel.INFO, 'RAP --->> interval = ' + interval);
						if (!l.Second_Notice_Sent__c) {
							compare = ltiB ? compare : integer.valueOf(tpc.Second_Notice_Interval__c);
							l.Time_in_First__c = GetInterval(l.Date_Submitted__c);
							if (l.Time_in_First__c >= compare) {
								ids2.add(l.Id);
								l.Second_Notice_Sent__c = true;
								l.Substage__c = 'Second_Notice_Sent';
								l.Date_Second_Sent__c = queryDate;
							}
							continue;
						}
						if (!l.Third_Notice_Sent__c) {
							l.Time_in_Second__c = GetInterval(l.Date_Second_Sent__c);
							compare = ltiB ? compare : integer.valueOf(tpc.Third_Notice_Interval__c);
							if (l.Time_in_Second__c >= compare) {
								ids3.add(l.Id);
								l.Third_Notice_Sent__c = true;
								l.Substage__c = 'Third_Notice_Sent';
								l.Date_Third_Sent__c = queryDate;
							}
							continue;
						}
						l.Time_in_Third__c = GetInterval(l.Date_Third_Sent__c);
						continue;
					}
		    		if (oldLiens.get(l.Id).Stages__c == 'Submitted') {
						if (!l.Second_Notice_Sent__c)
		    				l.Time_in_First__c = GetInterval(l.Date_Submitted__c);
						else if (!l.Third_Notice_Sent__c)
		    				l.Time_in_Second__c = GetInterval(l.Date_Second_Sent__c);
						else 
		    				l.Time_in_Third__c = GetInterval(l.Date_Third_Sent__c);
		    			if (l.Stages__c == 'Audit')
		    				l.Audit_Date__c = queryDate;
		    			if (l.Stages__c == 'Final')
		    				l.Final_Date__c = queryDate;
		    			continue;
		    		}
		    		if (oldLiens.get(l.Id).Stages__c == 'Audit')
		    			l.Time_in_Audit__c = GetInterval(l.Audit_Date__c);
		    		if (l.Stages__c == 'Final')
		    			l.Final_Date__c = queryDate;
		    	}
		    	if (!ids1.isEmpty())
		    		Send1(ids1);
		    	if (!ids2.isEmpty())
		    		Send2(ids2);
		    	if (!ids3.isEmpty())
		    		Send3(ids3);
		    }
		}    
	}
	
	private static integer GetInterval(Date input) {
		integer i = queryDay - input.dayOfYear();
		if (i >= 0)
			return i;
		return i+365;
	}
	
	public static void Send1(list<Id> lienIds) {
        list<Lien__c> liens = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, Lien_Type__c, 
									  Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
									  Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c
							   FROM Lien__c 
							   WHERE Id IN :lienIds];
		string template = system.label.FirstNotice;
        list<Messaging.SingleEmailMessage> mailList = new list<Messaging.SingleEmailMessage>();
        for (Lien__c l : liens) {
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	        mail.setTargetObjectId(l.Account__r.Lienholder_Contact__c);
	        mail.setWhatId(l.Id);
	        mail.setTemplateId(template);
	        mailList.add(mail);
        }
        if (!test.isRunningTest())
            Messaging.sendEmail(mailList);
	}
	
	public static void Send2(list<Id> lienIds) {
        list<Lien__c> liens = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, Lien_Type__c, 
									  Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
									  Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c
							   FROM Lien__c 
							   WHERE Id IN :lienIds];
		string template = system.label.SecondNotice;
        list<Messaging.SingleEmailMessage> mailList = new list<Messaging.SingleEmailMessage>();
        for (Lien__c l : liens) {
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	        mail.setTargetObjectId(l.Account__r.Lienholder_Contact__c);
	        mail.setWhatId(l.Id);
	        mail.setTemplateId(template);
	        mailList.add(mail);
        }
        if (!test.isRunningTest())
            Messaging.sendEmail(mailList);
	}

	public static void Send3(list<Id> lienIds) {
        list<Lien__c> liens = [SELECT Id, Audit_Date__c, Date_Submitted__c, Date_Second_Sent__c, Date_Third_Sent__c, Final_Date__c, Lien_Type__c, 
									  Second_Notice_Sent__c, Stages__c, Submitted__c, Third_Notice_Sent__c, Time_in_Third__c, Time_in_Audit__c, 
									  Time_in_First__c, Time_in_Second__c, Account__r.Lienholder_Contact__c
							   FROM Lien__c 
							   WHERE Id IN :lienIds];
		string template = system.label.ThirdNotice;
        list<Messaging.SingleEmailMessage> mailList = new list<Messaging.SingleEmailMessage>();
        for (Lien__c l : liens) {
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	        mail.setTargetObjectId(l.Account__r.Lienholder_Contact__c);
	        mail.setWhatId(l.Id);
	        mail.setTemplateId(template);
	        mailList.add(mail);
        }
        if (!test.isRunningTest())
            Messaging.sendEmail(mailList);
	}
}