/*
    Created: lmsw - February for Discovry/Design
    Purpose: PD-395 Create Controller extension to get Action Box Folder on Claimant page

History:    
    Updated: lmsw - February for Discovery/Design
    Purpose: PD-395 Change to handle classic and lightning, lightning is currently hardcoded

    Updated: RAP - March 2017 for March Release
    Purpose: refactor - pull action name from custom setting, some efficiency updates
    
*/
public with sharing class ActionBoxControllerExtension {
    
    public string folderID {get; set;}
	public boolean hasAction{get;set;}
	
    public ActionBoxControllerExtension(ApexPages.StandardController stdCtrl) {
        claimant__c c = (Claimant__c)stdCtrl.getRecord();
        hasAction = false;
    	ActionAssignment2__c act = ActionAssignment2__c.getInstance(userInfo.getUserId());
		if (act == null) return;
        // lightning detector
        if (isSalesForce1orLE()) {
            system.debug('Lightning');
            // Get Action name
			ActionBoxID__c abc = ActionBoxID__c.getInstance(act.Action__c);
			if (abc == null) return;
            folderID = abc.Box_Folder_ID__c;
            hasAction = true;
        }
        else {
            system.debug('Classic');
            // Get current AppName = Action then get Box Folder ID for that Action from Custom settings 
            list<Schema.DescribeTabSetResult> tabSetDesc = Schema.describeTabs();
            for (Schema.DescribeTabSetResult tsr : tabSetDesc) {
                if (tsr.isSelected()) {
                    string appName = tsr.getLabel();
                    ActionBoxID__c abc = ActionBoxID__c.getInstance(appName);
                    if (abc == null) return;
                    folderID = ActionBoxID__c.getValues(appName).box_folder_ID__c;
                    hasAction = true;
                }
            }
        }  
    }

    // Method to check if using Lightning
    public static boolean isSalesForce1orLE() {
        return (ApexPages.currentPage().getParameters().get('sfdcIFrameHost') != null ||
           		ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin') != null ||
           		ApexPages.currentPage().getParameters().get('isdtp') == 'p1');   
    }
}


