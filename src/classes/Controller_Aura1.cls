/*
    Created: RAP - January 2017 for March Release
    Purpose: PD-317 - Create Controller - Aura (Lien Detail Page) test
    
History:
    Updated: Lmsw - January 2017 for March Release
    Purpose: PD-400 - Lien - Dates Tab displaying Eligibility Dates
    
    Updated: Lmsw - January 2017 for March Release
    Purpose: PD-168 - Modified Claimant query.  Add schema for picklist

    Updated: Lmsw - January 2017 for March Release
    Purpose: PD-170 - Add query for Claimant

    Updated: RAP - January 2017 for March Release
    Purpose: PD-33 - Action Checklist

    Updated: RAP - January 2017 for March Release
    Purpose: PD-150 - Display data for just one Action
    
    Updated: RAP - January/February 2017 for March Release
    Purpose: PD-115 - Claimant - Lien Kanban
    
    Updated: RAP - January 2017 for March Release
    Purpose: PD-172 - Claimant - Injury Tab
             PD-173 - Claimant - Award Tab

    Updated: MJD - January 2017 for March Release
    Purpose: PD-401 - Lien - Override Final Lien Amount Component   
    
    Updated: RAP - January 2017 for March Release
    Purpose: refactor to store variables and limit database calls
    
    Updated: RAP - January 2017 for March Release
    Purpose: add method to return list of awards per Injury ID
    
    Updated: RAP - February 2017 for March Release
    Purpose: PD-190 - Claimant Summary - List of Claimaints

    Updated: lmsw - February 2017 for March Release
    Purpose: PRODSUPT-9 - Add Stage type map

    Updated: lmsw - February 2017 for March Release
    Purpose: PRODSUPT-2 - Add Law_Firm__r.Lead_Law_Firm__c to query
    
    Updated: RAP - February 2017 for March Release
    Purpose: PD-438 - Action - Law firms Tab

    Updated: lmsw - February 2017 for March Release
    Purpose: PD-437 - Add method to get count of Claimant status in an Action

    Updated: lmsw - February 2017 for March Release
    Purpose: PRODSUPT-2 - Add method to get Latest Eligible Disbursement Cylce for a Claimant
    
    Updated: Marc Dysart - March 2017 for March Release
    Purpose: PRODSUPT-10 - Add substage in Kanban View

    Updated: lmsw - March 2017 for March Release
    Purpose: PD-470 - Modify to add multiple contacts on a Lien
    
    Updated: RAP - March 2017 for March Release
    Purpose: PD-443 - Breadcrumbs in Lien & Claimant pages

	Updated: RAP - March 2017 for March Release
	Purpose: PD-440 - Action - Lien Tab - Alert Component
	
*/
global with sharing class Controller_Aura1 {

// public static final variables

    public static final string LIENQUERY = 'SELECT Account__c, Action__c, Claimant__c, Cleared__c, CreatedById, CreatedDate, Date_No_Interest__c, Date_Payment__c, ' +
                                           'Date_Re_sweep_Sent__c, Date_Submitted__c, Deal_Made__c, Eligible__c, Employer__c, End_Date__c, Final_Demand_Amount__c, ' +
                                           'Final_Demand_Received__c, Final_Lien_Amount_Value__c, Final_Lien_Amount__c, Final_Lien_Amt_Paid__c, Ledger__c, ' +
                                           'GroupHC_Name__c, Group_Name__c, Group_Number__c, HBAmount__c, HBPercent__c, Health_Plan__c, HICN__c, Id, ' +
                                           'LienAmount__c, Lien_Holder_File_ID__c, Lien_Type__c, Overridden_By__c, Overridden_By__r.Name, ' +
                                           'Medicare_Contractor__c, Medicare_Eligibility_Date__c, Military_Branch__c, Name, Notes__c, ' +
                                           'OffsetLienAmt__c, OffsetPercent__c, OtherPP_Type__c, OverrideFLAmount__c, OverrideReason__c, Override_Amount__c, ' +
                                           'Payable_Entity__c, Policy__c, Provider_Name__c, ReceivedSPD__c, RecordTypeId, ' +
                                           'Re_sweep_Sent__c, Stages__c, Start_Date__c, State__c, Status__c, Submitted__c, SubRightsConf__c, ' +
                                           'Subrogation_Rights__c, Subro_Rep__c, Substage__c, Treatment_Facility__c, Claimant__r.Name, Account__r.Lienholder_Contact__r.Name, ' +
                                           'Account__r.Name, Account__r.Lienholder_Contact__r.Email,  Account__r.Lienholder_Contact__r.Phone, Claimant__r.DOL__c, ' +
                                           'Account__r.Lienholder_Contact__r.Fax, Account__r.BillingStreet, Account__r.BillingCity, Account__r.BillingState, ' +
                                           'Account__r.BillingPostalCode, Account__r.Lienholder_Contact__c, Overridden_By_FMLA__c, Date_Overridden__c, ' +
                                           'Percent_Complete__c, ' +
                                           'Contact__r.Name, Contact__r.Email, Contact__r.Phone, Contact__r.Fax, Contact__r.MailingStreet, ' + //PD-470
                                           'Contact__r.MailingCity, Contact__r.MailingState, Contact__r.MailingPostalCode, ' +  //PD-470
                                           'Billing_Contact__r.Name, Billing_Contact__r.Email, Billing_Contact__r.Phone, Billing_Contact__r.Fax, ' + //PD-470
                                           'Billing_Contact__r.MailingStreet, Billing_Contact__r.MailingCity, Billing_Contact__r.MailingState, Billing_Contact__r.MailingPostalCode, ' +  //PD-470
                                           '(SELECT Id, Lien__c, Lien_Amount__c, Lien_Amount_Date__c, Phase__c FROM LienAmounts__r order by CreatedDate desc) ' + // PD-115
                                           'FROM Lien__c ';

// start PD-170 / PD-168 / PRODSUPT-2
    public static final string CLAIMANTQUERY = 'SELECT Address_1__c, Address_2__c, City__c, Claim_Admin_Id__c, Name, CreatedById, CreatedDate, DOB__c, DOD__c, DOL__c, Email__c, ' +
                                               'First_Name__c, Gender__c, LastModifiedById, LastModifiedDate, Last_Name__c, Law_Firm__c, Lawfirm_ID__c, Middle__c, ' +
                                               'Part_C_Resolution__c, Phone__c, Primary_Attorney__c, ProvidioID__c, Id, SSN__c, State__c, Status__c, Third_Party_ID__c, Venue__c, Zip__c, ' +
                                               'SSN_only__c, Status_Circle_Color__c, Law_Firm__r.Name, Law_Firm__r.Email__c, Law_Firm__r.Phone, ' +
                                               'Law_Firm__r.Lead_Law_Firm__r.Name, Law_Firm__r.Lead_Law_Firm__r.Email__c, Law_Firm__r.Lead_Law_Firm__r.Phone ' +
                                               'FROM Claimant__c ';
// end PD-170 / PD-168
// start PD-33 / PD-348
    public static final string ACTIONQUERY = 'SELECT Name, Active__c, All_Private_Liens__c, Asserted_Private_Liens_Only__c, Case_Name__c, Fee_Structure_Plus__c, ' +
                                             'Law_Firm__c, Law_Firm_Attorney_Contact__c, Law_Firm_Paralegal_Contact__c, MSP_Compliance__c, MT_as_SE__c, ' +
                                             'Medicaid_Eligibility_Verification__c, Medicare_Eligibility_Verification__c, Medicare_Model__c, Medicare_Part_C__c, ' +
                                             'Medicare_Single_Event__c, Medicare_Special_Project__c, OwnerId, PLRP__c, Providio_Generated__c, ' +
                                             'QSF_Administration__c, Id, SRP_Generated__c, Salesperson1__c, Salesperson2__c, Salesperson3__c, ' +
                                             'Unknown_follow_up_needed__c, NumClaimants__c, Law_Firm__r.Name, Law_Firm__r.BillingStreet, Law_Firm__r.BillingCity, ' +
                                             'Law_Firm__r.BillingState, Law_Firm__r.BillingPostalCode, Law_Firm_Attorney_Contact__r.Name, ' +
                                             'Law_Firm_Attorney_Contact__r.Phone, Law_Firm_Attorney_Contact__r.Fax FROM Action__c ';
// end PD-33 / PD-150
// start PD-172 / PD-173
    public static final string INJURYQUERY = 'SELECT Action__c, Compensable__c, Claimant__c, Description_Long__c, Explant_Date__c, ICD_Code__c, Id, Implant_Date__c, ' +
                                             'Injury_Category__c, Injury_Description__c, Last_Treatment_Date__c, Name, Non_Surgical_Treatment_Enhancer__c, ' +
                                             'Non_Surgical_Treatment__c, Surgical_Facility__c, Surgical_Treatment_Enhancer__c, Total_Surgery_Per_Defendant__c, ' +
                                             'Treatment_Enhancer__c, Base_Category__c, Settlement_Category__c, DOL__c, Start_Date__c, End_Date__c FROM Injury__c ';
// end PD172 / PD-173
// start PD-400
    public static final string LIENDATEQUERY = 'SELECT Date_End__c, Date_Start__c, Lien__c, LienType__c '+
                                               'FROM LienEligibilityDate__c ';
//end PD-400
// start refactor
    public static Lien__c lien{get;set;}
    public static list<Lien__c> liens{get;set;}
    public static map<string,list<Lien__c>> liensByInjury{get;set;}
    public static map<string,list<Lien__c>> lienMap{get;set;}
    public static map<string,list<Lien__c>> lienMap2{get;set;}
    public static map<string,Lien_Negotiated_Amounts__c> lienMap3{get;set;}
    public static map<string,list<LienEligibilityDate__c>> lienDateMap{get;set;}
    public static list<Lien_Negotiated_Amounts__c> lienAmtList{get;set;}
    public static map<string,list<Injury__c>> injuriesByLien{get;set;}
    public static map<string,list<Injury__c>> injuriesByClaimant{get;set;}
    public static map<string,list<Injury__c>> injuriesByAward{get;set;}
    public static map<string,list<Award__c>> awardsByLien{get;set;}
    public static map<string,list<Award__c>> awardsByClaimant{get;set;}
    public static map<string,list<Award__c>> awardsByInjury{get;set;}
    public static map<string,list<Claimant__c>> claimants{get;set;}
    public static list<Claimant__c> lfClaimants{get;set;}
    public static map<string,list<Claimant__c>> rfClaimants{get;set;}
    public static Claimant__c claimant{get;set;}
    public static list<string> claimantStatusPicklist{get;set;}
    public static list<Action__c> actions{get;set;}
    public static Action__c action{get;set;}
    public static list<string> stages{get;set;}
    public static map<string,string> stagesMap{get;set;}
    public static map<string,string> lienTypeMap{get;set;}
    public static map<string,string> lienStagesMap{get;set;}
    public static list<Account> lawFirms{get;set;} // PD-438
    public static map<string,string> lienSubstageMap{get;set;}
    public static map<String,Integer> statusCountMap{get;set;}  //PD-437
    public static map<String,Integer> distributionCountMap{get;set;}  //PD437
// end refactor
// constructor
    public Controller_Aura1(ApexPages.StandardController sc) {}
    
// action methods
// start PD-317    
@auraEnabled
    global static Lien__c getLien(string lId) {
        if (lien == null || lien.Id != lId)
            lien = database.query(LIENQUERY + 'WHERE Id = :lId limit 1');
        return lien;
    }
@auraEnabled
    global static list<Lien__c> getLiens(string cId, string aId) {
        if (liens == null)
            liens = database.query(LIENQUERY + 'WHERE Claimant__c = :cId');
        return liens;
    }
@auraEnabled
    global static map<string,list<Lien__c>> getLienMap(string aId) {
        if (lienMap == null) {
            lienMap = new map<string,list<Lien__c>>();
            list<Lien__c> lList = database.query(LIENQUERY + 'WHERE Action__c = :aId');
            for (Lien__c l : lList) {
                if (lienMap.containsKey(l.Lien_Type__c))
                    lienMap.get(l.Lien_Type__c).add(l);
                else
                    lienMap.put(l.Lien_Type__c, new list<Lien__c>{l});
            }
        }
        return lienMap;
    }
@auraEnabled
    global static map<string,string> getLienTypeMap() {
        if (lienTypeMap == null) {
            lienTypeMap = new map<string,string>();
            list<Schema.PicklistEntry> pickList = Lien__c.Lien_Type__c.getDescribe().getPicklistValues();
            for (Schema.PicklistEntry spe : pickList)
                lienTypeMap.put(spe.getValue(),spe.getLabel());
        }
        return lienTypeMap;
    }
// end PD-317
// start PRODSUPT-9
@auraEnabled
    global static map<string,string> getLienStageMap() {
        if (lienStagesMap == null) {
            lienStagesMap = new map<string,string>();
            list<Schema.PicklistEntry> pickList = Lien__c.Stages__c.getDescribe().getPicklistValues();
            for (Schema.PicklistEntry spe : pickList)
                lienStagesMap.put(spe.getValue(),spe.getLabel());
        }
        return lienStagesMap;
    }
@auraEnabled
    global static map<string,string> getLienSubstageMap() {
        if(lienSubstageMap == null) {
            lienSubstageMap = new map<string,string>();
            List<Schema.PicklistEntry> picklist = Lien__c.Substage__c.getDescribe().getPicklistValues();
            for(Schema.PicklistEntry spe : picklist)
                lienSubstageMap.put(spe.getValue(),spe.getLabel());
        }
        return lienSubstageMap;
    }
// end PRODSUPT-9
// start PD-115
@auraEnabled
    global static map<string,list<Lien__c>> getLienMap2(string cId) {
        if (lienMap2 == null) {
            lienMap2 = new map<string,list<Lien__c>>();
            list<Lien__c> lList = database.query(LIENQUERY + 'WHERE Claimant__c = :cId');
            string key;
            for (Lien__c l : lList) {
                key = l.Stages__c;
                if (lienMap2.containsKey(key))
                    lienMap2.get(key).add(l);
                else 
                    lienMap2.put(key, new list<Lien__c>{l});
            }
        }
        return lienMap2;
    }
@auraEnabled
    public static void updateLienCard(string stage, string lId) {
        if (lien == null || lien.Id != lId)
            lien = database.query(LIENQUERY + 'WHERE Id = :lId');
        lien.Stages__c = stage;
// start PRODSUPT -10
        lien.Substage__c = null;
// end PRODSUPT -10
        update lien;
    }

// end PD-115
// start PD-401
@auraEnabled
    public static Lien__c UpdateLien(string lId, string reason, decimal amount) {
        if (lien == null || lien.Id != lId)
            lien = getLien(lId);
        lien.Override_Amount__c = amount;
        lien.OverrideReason__c = reason;
        lien.Date_Overridden__c = system.now();
        lien.Overridden_By__c = userInfo.getUserId();
        update lien;
        lien = null;
        getLien(lId);
        return lien;
    }

// end PD-401
// start PD-400
@auraEnabled
    global static map<string,list<LienEligibilityDate__c>> getLienEligibleDates(string lId) {
        if (lienDateMap == null || lien.Id != lId) {
            lienDateMap = new map<string,list<LienEligibilityDate__c>>();
            list<LienEligibilityDate__c> dateList = database.query(LIENDATEQUERY + 'WHERE Lien__c =:lId order by Date_Start__c desc');
            for (LienEligibilityDate__c d : dateList) {
                system.debug('lien Date: '+ d);
                if (lienDateMap.containsKey(d.LienType__c))
                    lienDateMap.get(d.LienType__c).add(d);
                else
                    lienDateMap.put(d.LienType__c, new list<LienEligibilityDate__c>{d});
            }
            system.debug('Lien Dates Map: '+ lienDateMap);
        }
        return lienDateMap;
    }
// end PD-400
@auraEnabled 
    public static list<Lien_Negotiated_Amounts__c> getLienAmtList(string lId) {
        if (lienAmtList == null) {
            lienAmtList = [SELECT Id, Lien__c, Lien_Amount__c, Lien_Amount_Date__c, Phase__c
                           FROM Lien_Negotiated_Amounts__c
                           WHERE Lien__c = :lId
                           order by Lien_Amount_Date__c desc];
        }
        return lienAmtList;
    }
@auraEnabled
    public static list<Injury__c> getInjuries(string lId) {
        return getInjuriesByLien(lId);
    }   
@auraEnabled
    public static list<Injury__c> getInjuries2(string cId) {
        if (injuriesByClaimant == null)
            injuriesByClaimant = new map<string,list<Injury__c>>();
        if (!injuriesByClaimant.containsKey(cId)) {
            list<Injury__c> iList = database.query(INJURYQUERY + 'WHERE Claimant__c = :cId');
            injuriesByClaimant.put(cId, iList);
        }
        return injuriesByClaimant.get(cId);
    }   
@auraEnabled
    public static list<Award__c> getAwardsByLien(string lId) {
        if (awardsByLien == null)
            awardsByLien = new map<string,list<Award__c>>();
        if (!awardsByLien.containsKey(lId)) {
            list<Award__c> aList = [SELECT Id, Name, Action__c, Claimant__c, Date_of_Award__c, Amount__c, Description__c
            						FROM Award__c
            						WHERE Id IN (SELECT Award__c FROM AwardLien__c WHERE Lien__c = :lId)];
            awardsByLien.put(lId, aList);
        }
        return awardsByLien.get(lId);
    }
@auraEnabled
    public static list<Award__c> getAwards(string cId) {
        if (awardsByClaimant == null)
            awardsByClaimant = new map<string,list<Award__c>>();
        if (!awardsByClaimant.containsKey(cId)) {
            list<Award__c> aList = [SELECT Id, Action__c, Claimant__c, Date_of_Award__c, Amount__c, Description__c, Claimant__r.Name
                                    FROM Award__c
                                    WHERE Claimant__c = :cId];
            awardsByClaimant.put(cId, aList);
        }
        return awardsByClaimant.get(cId);
    }
// start PD-170
@auraEnabled
    global static Claimant__c getClaimant(string cId) {
        if (claimant == null || claimant.Id != cId)
            claimant = database.query(CLAIMANTQUERY + 'WHERE Id = :cId limit 1');
        return claimant;
    }

// end PD-170
@auraEnabled
    public static list<Claimant__c> getClaimants (string aId) {
        if (claimants == null)
            claimants = new map<string,list<Claimant__c>>();
        if (!claimants.containsKey(aId)) {              
            list<Claimant__c> cList = database.query(CLAIMANTQUERY + 'WHERE Id IN (SELECT Claimant__c FROM ActionClaimant__c WHERE Action__c = :aId)');
            claimants.put(aId, cList);
        }
        return claimants.get(aId);
    }
// start PD-168
@auraEnabled
    public static list<string> getClaimantStatusPicklist() {
        if (claimantStatusPicklist == null) {
            claimantStatusPicklist = new list<string>();
            list<Schema.PickListEntry> ple = Claimant__c.Status__c.getDescribe().getPicklistValues();
            for (Schema.PicklistEntry f : ple) {
                claimantStatusPicklist.add(f.getLabel());
            }
        }
        return claimantStatusPicklist;
    }
@auraEnabled
    public static Claimant__c updateClaimant(string cId, string status) {
        if (claimant == null || claimant.Id != cId)
            claimant = database.query(CLAIMANTQUERY + 'WHERE Id = :cId');
        claimant.Status__c = status;
        update claimant;
        return claimant;
    }
// end PD-168
// start PD-33
@auraEnabled
    public static list<Action__c> getActions() {
        if (actions == null)
            actions = database.query(ACTIONQUERY + 'WHERE Active__c = true');
        return actions;
    }
// end PD-33
// start PD-150
@auraEnabled
    public static Action__c getAction(string aId) {

        if (action == null)
            action = database.query(ACTIONQUERY + ' WHERE Id = :aId limit 1');
        return action;

    }
// end PD-150
// start PD-115
@auraEnabled
    public static list<string> getStages() {
        if (stages == null) {
            stages = new list<string>();
            list<Schema.PicklistEntry> pickList = Lien__c.Stages__c.getDescribe().getPicklistValues();
            for (Schema.PicklistEntry spe : pickList) {
                if (spe.isActive())
                    stages.add(spe.getLabel());
            }
        }
        return stages;
    }
@auraEnabled
    public static map<string,string> getStagesMap() {
        if (stagesMap == null) {
            stagesMap = new map<string,string>();
            list<Schema.PicklistEntry> pickList = Lien__c.Stages__c.getDescribe().getPicklistValues();
            for (Schema.PicklistEntry spe : pickList) {
                if (spe.isActive())
                    stagesMap.put(spe.getLabel(),spe.getValue());
            }
        }
        return stagesMap;
    }
// end PD-115
// start PD-172
@auraEnabled
    public static list<Injury__c> getInjuriesByLien(string lId) {
        if (injuriesByLien == null)
            injuriesByLien = new map<string,list<Injury__c>>();
        if (!injuriesByLien.containsKey(lId)) {
            list<Injury__c> iList = database.query(INJURYQUERY + 'WHERE Id IN (SELECT Injury__c FROM InjuryLien__c WHERE Lien__c = :lId)');
            injuriesByLien.put(lId, iList);
        }
        return injuriesByLien.get(lId);
    }
@auraEnabled
    public static list<Lien__c> getLiensByInjury(string iId) {
        if (liensByInjury == null)
            liensByInjury = new map<string,list<Lien__c>>();
        if (!liensByInjury.containsKey(iId)) {
            list<Lien__c> lList = database.query(LIENQUERY + 'WHERE Id IN (SELECT Lien__c FROM InjuryLien__c WHERE Injury__c = :iId)');
            liensByInjury.put(iId, lList);
        }
        return liensByInjury.get(iId);
    }
// end PD-172
// start PD-173
@auraEnabled
    public static list<Injury__c> getInjuriesByAward(string aId) {
        if (injuriesByAward == null)
            injuriesByAward = new map<string,list<Injury__c>>();
        if (!injuriesByAward.containsKey(aId)) {
            list<Injury__c> iList = database.query(INJURYQUERY + 'WHERE Id IN (SELECT Injury__c FROM InjuryAward__c WHERE Award__c = :aId)');
            injuriesByAward.put(aId, iList);
        }
        return injuriesByAward.get(aId);
    }
@auraEnabled
    public static map<string,list<Award__c>> getAwardsByInjuryMap(string cId) {
        map<Id,Injury__c> injuryMap = new map<Id,Injury__c>([SELECT Id, Name FROM Injury__c WHERE Claimant__c = :cId]);
        map<Id,Award__c> awardMap = new map<Id,Award__c>([SELECT Id, Action__c, Claimant__c, Date_of_Award__c, Amount__c, 
                                                                 Description__c, Claimant__r.Name
                                                          FROM Award__c
                                                          WHERE Claimant__c = :cId]);
        list<InjuryAward__c> iaList = [SELECT Injury__c, Award__c FROM InjuryAward__c WHERE Injury__c IN :injuryMap.keySet()];
        awardsByInjury = new map<string,list<Award__c>>();
        for (InjuryAward__c ia : iaList) {
            if (awardsByInjury.containsKey(ia.Injury__c))
                awardsByInjury.get(ia.Injury__c).add(awardMap.get(ia.Award__c));
            else
                awardsByInjury.put(ia.Injury__c, new list<Award__c>{awardMap.get(ia.Award__c)});
        }
        for (string key : awardsByInjury.keySet())
            system.debug(loggingLevel.INFO, 'RAP --->> map entry: ' + awardsByInjury.get(key));
        return awardsByInjury;
    }
@auraEnabled
    public static list<Award__c> getAwardsByInjury(string iId) {
        if (awardsByInjury == null)
            awardsByInjury = getAwardsByInjuryMap(claimant.Id);
        if (!awardsByInjury.containsKey(iId))
            return new list<Award__c>();
        return awardsByInjury.get(iId);
    }
// end PD-173
// start PD-438
@auraEnabled
    public static map<string,list<string>> getLawFirmMetrics(string aId) {
		lawFirms = [SELECT Id, Name, Phone, Email__c, Priority_Firm__c, RecordTypeId, ShippingCity, ShippingCountry, ShippingState, 
						   ShippingStreet, ShippingPostalCode,
						   (SELECT Id, Status__c FROM Claimants__r)
		            FROM Account
		            WHERE RecordType.Name = 'LawFirm'
		            AND Priority_Firm__c = true
		            AND Id IN (SELECT Account__c FROM Action_Account__c WHERE Action__c = :aId)
		            order by Name
		            limit 5];
        map<string,list<string>> tempMap = new map<string,list<string>>();
       	decimal i;
       	decimal j;
       	decimal k;
       	decimal l;
        if (lawFirms != null && !lawFirms.isEmpty()) {
	        for (Account a : lawFirms) {
	        	i = 0.0;
	        	j = 0.0;
	        	k = 0.0;
				if (a.Claimants__r != null && !a.Claimants__r.isEmpty()) {
					for (Claimant__c c : a.Claimants__r) {
		        		k++;
						if (c.Status__c == 'Cleared')
			        		i++;
						else
			        		j++;
					}
		        }
	        	l = k != 0 ? i/k*100 : 0.0;
	        	i = i.setScale(0);
	        	j = j.setScale(0);
	        	k = k.setScale(0);
	        	l = l.setScale(2);
	        	list<string> tempList = new list<string>{a.Id,string.valueOf(i),string.valueOf(j),string.valueOf(k),string.valueOf(l)};
	        	tempMap.put(a.Name, tempList);
	        }
        }
        return tempMap;
    }
// end PD-438
// start PD-317
@auraEnabled
    public static list<Claimant__c> getLFClaimants(string aId) {
        if (lfClaimants == null)
            lfClaimants = database.query(CLAIMANTQUERY + 'WHERE Law_Firm__c = :aId');
        return lfClaimants;
    }
@auraEnabled
    public static map<string,list<Claimant__c>> getOthers(string aId) {
        if (rfClaimants == null) {
            map<Id,Account> referMap;
            try {
                referMap = new map<Id,Account>([SELECT Id, Name FROM Account WHERE Lead_Law_Firm__c = :aId order by Name]);
            }
            catch (exception e){}
            if (referMap != null && !referMap.isEmpty()) {
                list<Id> tempList = new list<Id>();
                tempList.addAll(refermap.keySet());
                list<Claimant__c> temp = database.query(CLAIMANTQUERY + 'WHERE Law_Firm__c IN :tempList order by Law_Firm__r.Name');
                rfClaimants = new map<string,list<Claimant__c>>();
                for (Claimant__c c : temp) {
                    if (rfClaimants.containsKey(c.Law_Firm__r.Name))
                        rfClaimants.get(c.Law_Firm__r.Name).add(c);
                    else
                        rfClaimants.put(c.Law_Firm__r.Name, new list<Claimant__c>{c});
                }
            }
        }
        return rfClaimants;
    }
// end PD-317
// start PD-437
@auraEnabled
    public static map<string,integer> getClaimantStatusCount (string aId) {
        integer statusCount;
        statusCountMap = new map<string,integer>();
// Get Claimant status and count           
        for (Claimant__c c : [SELECT Id, Name, Status__c
                                    FROM Claimant__c
                                    WHERE Id IN (SELECT Claimant__c FROM ActionClaimant__c WHERE Action__c =: aId)
                                    ORDER BY Status__c]) {
            statusCount = 1;
            if (statusCountMap.containsKey(c.Status__c))
                statusCount += statusCountMap.get(c.Status__c);
            statusCountMap.put(c.Status__c,statusCount);
        }
        system.debug('Claimant Status Count: '+ statusCountMap);
        return statusCountMap;
    }
@auraEnabled 
    public static integer getClaimantsCountForAction (string aId) {
        return [SELECT count() FROM Claimant__c WHERE Action__c = :aId];
    }
@auraEnabled
    public static map<string,integer> getDistributionStatusCount (string aId) {
        integer statusCount;
        distributionCountMap = new map<String,Integer>();          
// Get Distribution status and count    
        for (Award__c aw : [SELECT Id, Name, Action__c, Distribution_Status__c 
        					FROM Award__c 
        					WHERE Action__c = :aId 
        					ORDER BY Distribution_Status__c ]) {
			statusCount = 1;
            if (distributionCountMap.containsKey(aw.Distribution_Status__c))
                statusCount += distributionCountMap.get(aw.Distribution_Status__c);
            distributionCountMap.put(aw.Distribution_Status__c,statusCount);
        }
        system.debug('Distribution Status Count: '+ distributionCountMap);
        return distributionCountMap;
    }
@auraEnabled 
    public static integer getAwardsCountForAction (string aId) {
        return [SELECT count() FROM Award__c WHERE Action__c = :aId];
    }
// end PD-437
// start PD-159
@auraEnabled
    public static map<string,list<string>> getStageMetrics(string aId) {
		liens = [SELECT Id, Name, Stages__c, Lien_Type__c
				 FROM Lien__c
		         WHERE Action__c = :aId];
        map<string,list<string>> tempMap = new map<string,list<string>>();
        if (liens != null && !liens.isEmpty()) {
			map<string,list<Lien__c>> typeMap = new map<string,list<Lien__c>>();
			for (Lien__c l : liens) {
				if (typeMap.containsKey(l.Lien_Type__c))
					typeMap.get(l.Lien_Type__c).add(l);
				else
					typeMap.put(l.Lien_Type__c, new list<Lien__c>{l});
			}
			map<string,string> lienTypes = getLienTypeMap();
			map<string,string> lienStages = getLienStageMap();
			map<string,integer> stageMap;
	        list<string> tempList;
	        for (string t : lienTypes.keySet()) {
				list<Lien__c> lienList = typeMap.get(t);
				tempList = new list<string>();
				stageMap = new map<string,integer>();
		        if (lienList != null) {
			        for (Lien__c l : lienList) {
						integer i = 1;
						if (stageMap.containsKey(l.Stages__c))
							i += stageMap.get(l.Stages__c);
						stageMap.put(l.Stages__c, i);
		        	}
		        }
	        	for (string stage : lienStages.keySet()) {
	        		if (stageMap.containsKey(stage))
	        			tempList.add(string.valueOf(stageMap.get(stage)));
	        		else
	        			templist.add('0');
	        	}
	        	tempMap.put(lienTypes.get(t), tempList);
	        }
        }
        return tempMap;
    }
// end PD-159
// start PRODSUPT-2
@auraEnabled
    public static Release__c getLastRelease(string cId) {
        return [SELECT Id, Name, Status__c, Date__c, Award__c, Award__r.Name, Award__r.Claimant__c
                FROM Release__c
                WHERE Award__r.Claimant__c = :cId
                order by Date__c desc
                limit 1];
    }
// end PRODSUPT-2
// start PD-443
@auraEnabled
	public static list<string> getAction2() {
		system.debug(loggingLevel.INFO, 'RAP --->> UserId = ' + userInfo.getUserId());
		string act = ActionAssignment2__c.getInstance(userInfo.getUserId()).Action__c;
		string name = [SELECT Name FROM Action__c WHERE Id = :act].Name;
		return new list<string>{act,name};
	}
// end PD-443
// start PD-440
@auraEnabled
	public static list<string> getAlerts() {
		string act = ActionAssignment2__c.getInstance(userInfo.getUserId()).Action__c;
		list<Claimant__c> cList = [SELECT Id, Status__c, AwardAmtRemaining__c, LienAmtRemaining__c,
										  (SELECT Amount_Remaining__c, Distribution_Status__c FROM Awards__r),
										  (SELECT Total_Fees_Applied__c, Stages__c FROM Liens__r)
								   FROM Claimant__c
								   WHERE Id IN (SELECT Claimant__c FROM ActionClaimant__c WHERE Action__c = :act)];
		integer a1 = 0;
		integer a2 = 0;
		integer a3 = 0;
		integer a4 = 0;
		for (Claimant__c c : cList) {
			if (c.Status__c == 'Cleared') {
				if (c.Liens__r == null || c.Liens__r.isEmpty())
					a1++;
				else {
					decimal d = c.LienAmtRemaining__c;
					for (Lien__c l : c.Liens__r)
						d += l.Total_Fees_Applied__c;
					if (c.AwardAmtRemaining__c < d)
						a2++;
					if (d == c.LienAmtRemaining__c)
						a4++;
				}
			}
			else {
				boolean add = true;
				for (Lien__c l : c.Liens__r) {
					if (l.Stages__c != 'Final') {
						add = false;
						break;
					}
				}
				if (add)
					a3++;
			}
		}
		system.debug(loggingLevel.INFO, 'RAP --->> alerts = ' + a1 + ' -- ' + a2 + ' -- ' + a3 + ' -- ' + a4);
		return new list<string>{string.valueOf(a1),string.valueOf(a2),string.valueOf(a3),string.valueOf(a4)};
	}
// end PD-440
}